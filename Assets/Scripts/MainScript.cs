﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainScript : MonoBehaviour
{
    static public MainScript instance;

    public Button[] LanguageSwitchBtns;
    public Animator ListModeAnimator;
    public Animator DetailModeAnimator;
    public DetailProductView detailProductView;
    public CameraOrbit cameraOrbit;
    public Entity_English ExcelData;

    public GameObject ListContainer;
    public GameObject LeftTypeListItemPrefab;
    public GameObject RightTypeListItemPrefab;
    public Sprite[] Icons;
    public GameObject[] Models;

    public int LanguageIndex = 0;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        InitSettings();
    }

    void InitSettings()
    {
        Screen.SetResolution(1080, 1920, true);
        ExcelData = ExcelImportRunTime.ReadExcel(ExcelData, "Data.xlsx", new string[1] { "Eng"});
        InitList();
        ShowModelByIndex(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitList()
    {
        for (int i = 0; i < ListContainer.transform.childCount; i++)
        {
            Destroy(ListContainer.transform.GetChild(i).gameObject);
        }
        int k = 0;
        foreach (Entity_English.ProjectParam prod in ExcelData.sheets[0].TextValueslist)
        {
            GameObject prefab = RightTypeListItemPrefab; 
            if (k % 2 == 0)
            {
                prefab = LeftTypeListItemPrefab;
            }
            GameObject ProductItem = Instantiate(prefab, ListContainer.transform);
            ProductItem.GetComponent<ProductItem>().SetData(prod, k);
            k++;
        }
    }

    public void SwitchLanguageByIndex(int _index)
    {
        foreach (Button btn in LanguageSwitchBtns)
        {
            btn.interactable = true;
        }
        LanguageSwitchBtns[_index].interactable = false;
        LanguageIndex = _index;
        InitList();
        detailProductView.SwitchLanguage();
    }

    public void ShowDetailPanel(Entity_English.ProjectParam _data, int _index, RectTransform _rectTransform)
    {
        float delay = StartHideAnim(_index);
        cameraOrbit.ResetPosition();
        detailProductView.SetData(_data);
        detailProductView.SwitchDetailViewModeByIndex(0);
        DetailModeAnimator.gameObject.SetActive(true);
        bool left = true;
        if (_index % 2 != 0)
        {
            left = false;
        }
        detailProductView.StartTitleAnim(_rectTransform, delay - 0.1f, left);

        Loom.QueueOnMainThread(() => {
            
            DetailModeAnimator.SetFloat("Direction", 1);
            DetailModeAnimator.Play("ShowDetailPanel", -1, 0);
        }, 0.95f);

        Loom.QueueOnMainThread(() => {
            ListModeAnimator.gameObject.SetActive(false);
        }, delay + 0.05f);
    }

    public void BackToProductList()
    {
        cameraOrbit.CanUse = false;
        
        DetailModeAnimator.SetFloat("Direction", -1);
        DetailModeAnimator.Play("ShowDetailPanel", -1, 1);
        Loom.QueueOnMainThread(() => {
            DetailModeAnimator.gameObject.SetActive(false);
        }, 1f);

        Loom.QueueOnMainThread(() => {
            ListModeAnimator.gameObject.SetActive(true);
            ListModeAnimator.SetFloat("Direction", 1);
            ListModeAnimator.Play("ShowPanel", -1, 0);
            StartShowAnim(0);
        }, 0.85f);

        /*ListModeAnimator.gameObject.SetActive(true);
        ListModeAnimator.SetFloat("Direction", 1);
        ListModeAnimator.Play("ShowPanel", -1, 0);*/
    }

    public void ShowModelByIndex(int _index)
    {
        foreach (GameObject model in Models)
        {
            model.SetActive(false);
        }
        Models[_index].SetActive(true);
    }

    

    public float StartHideAnim(int _index)
    {
        float upDelay = 0;
        ListContainer.transform.GetChild(_index).localScale = new Vector3(ListContainer.transform.GetChild(_index).localScale.x, 0, ListContainer.transform.GetChild(_index).localScale.z);
        if (_index - 1 >= 0)
        {
            for (int i = _index - 1; i >= 0; i--)
            {
                ListContainer.transform.GetChild(i).GetComponent<ProductItem>().CanUse = false;
                iTween.ScaleTo(ListContainer.transform.GetChild(i).gameObject, iTween.Hash("y", 0, "time", .3f, "delay", upDelay, "easetype", iTween.EaseType.easeOutCubic));
                upDelay += 0.1f;
            }
        }
        float downDelay = 0;
        if (_index + 1 < ListContainer.transform.childCount)
        {
            for (int i = _index; i < ListContainer.transform.childCount; i++)
            {
                ListContainer.transform.GetChild(i).GetComponent<ProductItem>().CanUse = false;
                iTween.ScaleTo(ListContainer.transform.GetChild(i).gameObject, iTween.Hash("y", 0, "time", .3f, "delay", downDelay, "easetype", iTween.EaseType.easeOutCubic));
                downDelay += 0.1f;
            }
        }
        return (upDelay < downDelay ? downDelay : upDelay) + 0.1f;
    }

    public void StartShowAnim(int _index)
    {
        float delay = 0;
        for (int i = _index; i >= 0; i--)
        {
            ListContainer.transform.GetChild(i).GetComponent<ProductItem>().CanUse = true;
            iTween.ScaleTo(ListContainer.transform.GetChild(i).gameObject, iTween.Hash("y", 1, "time", .3f, "delay", delay, "easetype", iTween.EaseType.easeOutCubic));
            delay += 0.1f;
        }
        delay = 0;
        for (int i = _index; i < ListContainer.transform.childCount; i++)
        {
            ListContainer.transform.GetChild(i).GetComponent<ProductItem>().CanUse = true;
            iTween.ScaleTo(ListContainer.transform.GetChild(i).gameObject, iTween.Hash("y", 1, "time", .3f, "delay", delay, "easetype", iTween.EaseType.easeOutCubic));
            delay += 0.1f;
        }
    }
}
