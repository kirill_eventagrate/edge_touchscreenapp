﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

public class ExcelImportRunTime 
{

    //private string filePath = "Assets/ExelData/Navigation_v02.xlsx";
    //static string root = "";

    

    static public Entity_English ReadExcel(Entity_English _exelData, string _fileName, string[] _sheetNames)
    {
        //sheetNames.Clear();
        _exelData.sheets.Clear();

        foreach (string _sheetName in _sheetNames)
        {
            //root = Application.persistentDataPath + "/";
            string filePath = _fileName;
            using (FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                IWorkbook book = null;
                if (Path.GetExtension(filePath) == ".xls")
                {
                    book = new HSSFWorkbook(stream);
                }
                else
                {
                    book = new XSSFWorkbook(stream);
                }
                //book.GetSheet
                string sheetName = _sheetName;
                ISheet sheet = book.GetSheet(sheetName);
                if (sheet == null)
                {
                    Debug.LogError("[QuestData] sheet not found:" + sheetName);
                    return null;
                }

                Entity_English.Sheet s = new Entity_English.Sheet();
                s.name = sheetName;

                for (int i = 1; i <= sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);
                    ICell cell = null;

                    Entity_English.ProjectParam p = new Entity_English.ProjectParam();

                    //cell = row.GetCell(0); p.Text_id = (cell == null ? "" : cell.NumericCellValue.ToString());
                    cell = row.GetCell(0); p.Title = (cell == null ? "" : cell.StringCellValue);
                    cell = row.GetCell(1); p.Description = (cell == null ? "" : cell.StringCellValue);
                    cell = row.GetCell(2); p.ArabTitle = (cell == null ? "" : cell.StringCellValue);
                    cell = row.GetCell(3); p.ArabDescription = (cell == null ? "" : cell.StringCellValue);
                    cell = row.GetCell(4); p.IconID = System.Convert.ToInt32((cell == null ? 0d : cell.NumericCellValue));
                    cell = row.GetCell(5); p.ModelID = System.Convert.ToInt32((cell == null ? 0d : cell.NumericCellValue));
                    s.TextValueslist.Add(p);
                }
                _exelData.sheets.Add(s);
            }
        }


        return _exelData;
    }
}
