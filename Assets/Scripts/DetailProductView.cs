﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UPersian.Components;

public class DetailProductView : MonoBehaviour
{
    public RectTransform titleRectTransform;
    public DetailTitleScript LeftTitleType;
    public DetailTitleScript RightTitleType;
    public Image DetailImage;
    public Text Description;
    public RtlText ArabDescription;

    public RectTransform DescriptionContainer;

    public Button[] DetailModeSwitchBtns;
    public GameObject[] DetailModeView;

    //public Button ViewPortBtn;
    public CameraOrbit cameraOrbit;

    Entity_English.ProjectParam Data;
    Vector2 StartTitlePos = Vector2.zero;
    Vector2 StartSize = Vector2.zero;
    // Start is called before the first frame update
    void Start()
    {
        StartTitlePos = titleRectTransform.anchoredPosition;
        StartSize = titleRectTransform.sizeDelta;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartTitleAnim(RectTransform _transform, float _delay, bool left)
    {
        StartTitlePos = titleRectTransform.anchoredPosition;
        StartSize = titleRectTransform.sizeDelta;
        if (left)
        {
            LeftTitleType.gameObject.SetActive(true);
            RightTitleType.gameObject.SetActive(false);
        }
        else
        {
            LeftTitleType.gameObject.SetActive(false);
            RightTitleType.gameObject.SetActive(true);
        }
        gameObject.SetActive(true);
        
        titleRectTransform.anchoredPosition = _transform.anchoredPosition;
        titleRectTransform.sizeDelta = _transform.sizeDelta;
        iTween.ValueTo(titleRectTransform.gameObject, iTween.Hash(
            "from", _transform.anchoredPosition,
            "to", StartTitlePos,
            "time", 0.4f,
            "delay", _delay,
            "onupdatetarget", this.gameObject,
            "onupdate", "MoveTitle",
            "easetype", iTween.EaseType.easeOutSine));

        iTween.ValueTo(titleRectTransform.gameObject, iTween.Hash(
            "from", _transform.sizeDelta,
            "to", StartSize,
            "time", 0.4f,
            "delay", _delay,
            "onupdatetarget", this.gameObject,
            "onupdate", "ChangeSize",
            "easetype", iTween.EaseType.easeOutSine));
    }

    public void MoveTitle(Vector2 position)
    {
        titleRectTransform.anchoredPosition = position;
    }

    public void ChangeSize(Vector2 _val)
    {
        titleRectTransform.sizeDelta = _val;
    }

    public void SetData(Entity_English.ProjectParam _data)
    {
        Data = _data;
        LeftTitleType.SetData(_data);
        RightTitleType.SetData(_data);
        DetailImage.sprite = MainScript.instance.Icons[Data.IconID];
        Description.text = Data.Description;
        ArabDescription.text = Data.ArabDescription;
        MainScript.instance.ShowModelByIndex(Data.ModelID);
        UpdateTextFieldSize();
    }

    public void SwitchDetailViewModeByIndex(int _index)
    {
        foreach (Button btn in DetailModeSwitchBtns)
        {
            btn.interactable = true;
        }
        foreach (GameObject viewMode in DetailModeView)
        {
            viewMode.SetActive(false);
        }
        DetailModeView[_index].SetActive(true);
        DetailModeSwitchBtns[_index].interactable = false;
        if (_index == 1)
        {
            cameraOrbit.CanUse = true;
        }
        else
        {
            cameraOrbit.CanUse = false;
        }
    }

    public void SwitchLanguage()
    {
        LeftTitleType.SwitchLanguage();
        RightTitleType.SwitchLanguage();
        if (MainScript.instance.LanguageIndex == 0)
        {
            Description.gameObject.SetActive(true);
            ArabDescription.gameObject.SetActive(false);
        }
        else
        {
            Description.gameObject.SetActive(false);
            ArabDescription.gameObject.SetActive(true);
        }
        UpdateTextFieldSize();
    }

    public void UpdateTextFieldSize()
    {
        Loom.QueueOnMainThread(() => {
            float Height = 0;
            if (MainScript.instance.LanguageIndex == 0)
            {
                Height = Description.gameObject.GetComponent<RectTransform>().rect.height;
            }
            else
            {
                Height = ArabDescription.gameObject.GetComponent<RectTransform>().rect.height;
            }
            DescriptionContainer.sizeDelta = new Vector2(DescriptionContainer.sizeDelta.x, Height);
            //Debug.Log("width: " + DescriptionContainer.sizeDelta.x + " height: " + Height);
        }, 0.2f);
    }
}
