﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UPersian.Components;

public class ProductItem : MonoBehaviour
{
    public Image Icon;
    public Text Title;
    public RtlText ArabTitle;
    public bool CanUse = true;

    int Index = 0;
    Entity_English.ProjectParam Data;
    Button btn;

    // Start is called before the first frame update
    void Start()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(() => {
            if (CanUse)
            {
                MainScript.instance.ShowDetailPanel(Data, Index, gameObject.GetComponent<RectTransform>());
                //MainScript.instance.StartHideAnim(Index);
            }
        });
        if (MainScript.instance.LanguageIndex == 0)
        {
            Title.gameObject.SetActive(true);
            ArabTitle.gameObject.SetActive(false);
        }else
        {
            Title.gameObject.SetActive(false);
            ArabTitle.gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetData(Entity_English.ProjectParam _data, int _index)
    {
        Index = _index;
        Data = _data;
        Icon.sprite = MainScript.instance.Icons[Data.IconID];
        Title.text = Data.Title;
        ArabTitle.text = Data.ArabTitle;
    }
}
