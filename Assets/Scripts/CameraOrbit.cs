﻿using UnityEngine;
using System.Collections;

public class CameraOrbit : MonoBehaviour
{
    public float MinDistance = 1.0f;
    public float MaxDistance = 1.3f;
    float distance = 2;
    float distanceTarget;
    Vector2 mouse;
    Vector2 mouseOnDown;
    Vector2 rotation;
    public Vector2 target = new Vector2(Mathf.PI * 3 / 2, Mathf.PI / 6);
    public Vector2 targetOnDown;

    public bool moveTowardsDestination = false;
    public bool moveTowardsOrigin = false;

    public Transform targetCountry;
    public bool down = false;
    public bool CanUse = false;

    // Use this for initialization
    void Start()
    {
        distanceTarget = transform.position.magnitude;
    }
    
    //bool fadeOnce = true;

    //float elapsedTime = 0;

    // Update is called once per frame
    void Update()
    {
        if (CanUse)
        {
            /*if (Input.GetMouseButtonDown(0))
            {
                mouseOnDown.x = Input.mousePosition.x;
                mouseOnDown.y = -Input.mousePosition.y;

                targetOnDown.x = target.x;
                targetOnDown.y = target.y;
                down = true;
            }
            else*/
            if (Input.GetMouseButtonUp(0))
            {
                down = false;
            }
            if (down)
            {
                mouse.x = Input.mousePosition.x;
                mouse.y = -Input.mousePosition.y;

                float zoomDamp = distance / 1;

                target.x = targetOnDown.x + (mouse.x - mouseOnDown.x) * 0.001f * zoomDamp;
                target.y = targetOnDown.y + (mouse.y - mouseOnDown.y) * 0.001f * zoomDamp;

                //target.y = Mathf.Clamp(target.y, -Mathf.PI / 2 + 0.01f, Mathf.PI / 2 - 0.01f);

                //target.y = Mathf.Clamp(target.y, 0.08f, 0.95f);
            }

            
        }
        //distanceTarget -= Input.GetAxis("Mouse ScrollWheel");
        //Debug.Log("distanceTarget" + distanceTarget);
        distanceTarget = Mathf.Clamp(distanceTarget, MinDistance, MaxDistance);

        rotation.x += (target.x - rotation.x) * 0.1f;
        rotation.y += (target.y - rotation.y) * 0.1f;
        distance += (distanceTarget - distance) * 0.3f;
        Vector3 position;
        position.x = distance * Mathf.Sin(rotation.x) * Mathf.Cos(rotation.y);
        position.y = distance * Mathf.Sin(rotation.y);
        position.z = distance * Mathf.Cos(rotation.x) * Mathf.Cos(rotation.y);
        //Debug.Log(position);
        transform.position = position;
        transform.LookAt(Vector3.zero);
    }

    public void ResetPosition()
    {
        target = new Vector2(Mathf.PI * 3 / 2, Mathf.PI / 6);
    }

    public void SetTarget(Vector2 _target)
    {
        target = _target;
    }

    public void StartToRotate()
    {
        mouseOnDown.x = Input.mousePosition.x;
        mouseOnDown.y = -Input.mousePosition.y;

        targetOnDown.x = target.x;
        targetOnDown.y = target.y;
        down = true;
    }
}