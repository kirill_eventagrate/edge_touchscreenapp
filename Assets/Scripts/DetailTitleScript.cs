﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UPersian.Components;

public class DetailTitleScript : MonoBehaviour
{
    public Image TitleIcon;
    public Text TitleText;
    public RtlText ArabTitle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetData(Entity_English.ProjectParam _data)
    {
        TitleIcon.sprite = MainScript.instance.Icons[_data.IconID];
        TitleText.text = _data.Title;
        ArabTitle.text = _data.ArabTitle;
    }

    public void SwitchLanguage()
    {
        if (MainScript.instance.LanguageIndex == 0)
        {
            TitleText.gameObject.SetActive(true);
            ArabTitle.gameObject.SetActive(false);
            
        }
        else
        {
            TitleText.gameObject.SetActive(false);
            ArabTitle.gameObject.SetActive(true);
        }
        
    }
}
