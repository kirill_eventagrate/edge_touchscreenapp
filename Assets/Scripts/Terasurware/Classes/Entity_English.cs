using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_English : ScriptableObject
{	
	public List<Sheet> sheets = new List<Sheet> ();

    [System.SerializableAttribute]
	public class Sheet
	{
		public string name = string.Empty;
		public List<ProjectParam> TextValueslist = new List<ProjectParam>();
    }

    [System.SerializableAttribute]
	public class ProjectParam
	{
		//public string Text_id;
		public string Title;
		public string Description;
        public string ArabTitle;
        public string ArabDescription;
        public int IconID;
        public int ModelID;
    }

    public ProjectParam GetDataById(int _id)
    {
        return sheets[0].TextValueslist[_id - 2];
    }
}

